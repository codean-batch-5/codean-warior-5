# Bootcamp Batch 5

Selamat datang di Bootcamp Batch 5! Dalam bootcamp ini, kita akan fokus pada pengembangan aplikasi web dengan dua track utama: Frontend (FE) dan Backend (BE). Setiap track memiliki branch terpisah untuk memudahkan pengelolaan dan pengembangan.

## Struktur Branch

- **Frontend (FE)**: `branch-fe`
- **Backend (BE)**: `branch-be`

## Struktur Folder

- **frontend-space**: Folder untuk kode frontend.
- **backend-space**: Folder untuk kode backend.

## Getting Started

Untuk memulai, pastikan Anda sudah memiliki Git terinstal di sistem Anda. Clone repository ini, kemudian checkout ke branch yang sesuai dengan track yang Anda ikuti.


### Frontend (FE)
Track Frontend akan fokus pada pengembangan antarmuka pengguna dengan teknologi berikut:

HTML, CSS, dan JavaScript
Framework: React.js

### Backend (BE)
Track Backend akan fokus pada pengembangan logika server dan database dengan teknologi berikut:

Bahasa Pemrograman: Java
Framework: Spring Boot
Database: PostgreSQL

### Clone Repository

```
git clone https://gitlab.com/codean-warrior/codean-warrior-5.git
cd codean-warrior-5
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/codean-batch-5/codean-warior-5.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/codean-batch-5/codean-warior-5/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Project status
Setiap progres dan proses yang telah dibuat jangan lupa melakukan commit dan push ke gitlab. berikan deskripsi singkat untuk setiap commit sebelum di push. 


## Booster
Selamat berjuang para peserta Bootcamp **Batch 5**! 
**Ingat**, _perjalanan ini mungkin penuh tantangan, tetapi setiap tantangan adalah peluang untuk belajar dan berkembang_. Jangan takut untuk mencoba hal baru, dan jangan ragu untuk bertanya jika ada yang kurang jelas. Anda semua luar biasa, dan kami percaya bahwa Anda akan mencapai hasil yang mengagumkan!
